package Main;

import GameInfrastructure.GameManager;
import SupGame.Games.ConsoleSup.ConsoleSup;

/**
 * Created by yakir on 7/27/16.
 */
public class Program {
    public static void main(String[] args) {

        // Long version
        ConsoleSup consoleSup = new ConsoleSup();
        GameManager gameManager = new GameManager(consoleSup);
        gameManager.Run();

        // Short version
        // (new GameManager(new ConsoleSup())).Run();
    }
}
