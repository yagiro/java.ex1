package SupGame.Utils;

import Generated.GameDescriptor;
import com.sun.istack.internal.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by yakir on 8/9/16.
 *
 */

public class XmlUtils {

    @Nullable
    public static GameDescriptor ParseGameDescriptorFromXml(String i_XmlFilePath) throws JAXBException {
        System.out.println(System.getProperty("line.separator") + "From File to GameDescriptor");

        GameDescriptor gameDescriptor = null;

        File xmlFile = new File(i_XmlFilePath);
        JAXBContext jaxbContext = JAXBContext.newInstance(GameDescriptor.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        gameDescriptor = (GameDescriptor) jaxbUnmarshaller.unmarshal(xmlFile);

        return gameDescriptor;
    }
}
