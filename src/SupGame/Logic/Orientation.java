package SupGame.Logic;

/**
 * Created by yakir on 8/9/16.
 *
 */
public enum Orientation {
    Row, Column;

    public static Orientation Parse(String i_ToParse) {
        i_ToParse = i_ToParse.toLowerCase();

        switch (i_ToParse) {
            case "row":
                return Orientation.Row;
            case "column":
                return Orientation.Column;
            default:
                // todo: throw special kind of exception with description
                throw new RuntimeException("Slice orientation value (row/column) is not valid: " + i_ToParse);
        }
    }
}
