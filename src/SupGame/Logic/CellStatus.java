package SupGame.Logic;

/**
 * Created by yakir on 8/2/16.
 */
public enum CellStatus {
    Filled {
        @Override
        public String getConsoleRepresentaion() {
            return " # ";
        }
    }, Blank {
        @Override
        public String getConsoleRepresentaion() {
            return "   ";
        }
    }, Undefined {
        @Override
        public String getConsoleRepresentaion() {
            return " . ";
        }
    };

    public abstract String getConsoleRepresentaion();
}
