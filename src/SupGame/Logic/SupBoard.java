package SupGame.Logic;

import javax.annotation.Generated;
import Generated.GameDescriptor;
import Generated.Slices;
import Generated.Slice;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by yakir on 7/27/16.
 *
 */
public class SupBoard {

    private CellStatus[][] m_Cells;
    private Streaks m_RowStreaks, m_ColStreaks;

    public SupBoard() {
        m_RowStreaks = new Streaks();
        m_ColStreaks = new Streaks();
    }

    public SupBoard(GameDescriptor.Board i_RawBoard) {
        this();
        createAndResetBoard(i_RawBoard.getDefinition().getRows().intValue(), i_RawBoard.getDefinition().getColumns().intValue());
        populateStreaks(i_RawBoard.getDefinition().getSlices());
    }

    public Streaks getRowStreaks() {
        return m_RowStreaks;
    }

    public Streaks getColStreaks() {
        return m_ColStreaks;
    }

    private void populateStreaks(Slices i_Slices) {
        m_RowStreaks.GetStreaks(i_Slices, Orientation.Row);
        m_ColStreaks.GetStreaks(i_Slices, Orientation.Column);
    }

    private void createAndResetBoard(int i_Rows, int i_Cols) {
        m_Cells = new CellStatus[i_Rows][i_Cols];

        for (int i = 0; i < i_Rows; ++i) {
            for (int j = 0; j < i_Cols; ++j) {
                m_Cells[i][j] = CellStatus.Undefined;
            }
        }
    }

    public Integer getNumOfRows() {
        return m_RowStreaks.getStreaks().size();
    }

    public Integer getNumOfCols() {
        return m_ColStreaks.getStreaks().size();
    }

    public CellStatus[][] getCells() {
        return m_Cells;
    }






    public class Streaks {
        private ArrayList<Streak> m_Streaks;

        public Streaks() {
            m_Streaks = new ArrayList<>();
        }

        public void GetStreaks(Slices i_Slices, Orientation i_Orientation) {
            List<Slice> slices = i_Slices.getSlice();
            for (Slice slice : slices) {
                if (Orientation.Parse(slice.getOrientation()).equals(i_Orientation)) {
                    m_Streaks.add(new Streak(slice.getBlocks().replaceAll("\\s+", ""), slice.getId().intValue()));
                }
            }
            Collections.sort(m_Streaks, new StreakIdComparator());

        }

        public int getMaxStreakLength(){
            int max = 0;
            for(Streak streak : this.getStreaks()){
                if(max < streak.getStreakLengths().length){
                    max = streak.getStreakLengths().length;
                }
            }

            return max;
        }

        public ArrayList<Streak> getStreaks() {
            return m_Streaks;
        }


    }

    public class Streak {

        private int[] m_StreakLengths;
        private int m_Id;

        public Streak(int i_Length) {
            m_StreakLengths = new int[i_Length];
        }

        public Streak(String i_StreakStr, int i_Id) {
            // i_StreakStr has been validated
            m_Id = i_Id;
            String[] streakLengthsStrings = i_StreakStr.split(",");
            m_StreakLengths = new int[streakLengthsStrings.length];

            int i = 0;
            for (String lengthStr : streakLengthsStrings) {
                m_StreakLengths[i] = Integer.parseInt(lengthStr);
                ++i;
            }
        }

        public int[] getStreakLengths() {
            return m_StreakLengths;
        }


        public int getId() {
            return m_Id;
        }


    }
}
