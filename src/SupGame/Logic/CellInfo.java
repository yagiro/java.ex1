package SupGame.Logic;

/**
 * Created by yakir on 8/2/16.
 */
public class CellInfo {

    private int m_Row, m_Col;
    private CellStatus m_CellStatus;

    public CellInfo(int i_Row, int i_Col, CellStatus i_CellStatus) {
        m_Row = i_Row;
        m_Col = i_Col;
        m_CellStatus = i_CellStatus;
    }

    public int getRow() {
        return m_Row;
    }

    public int getCol() {
        return m_Col;
    }

    public CellStatus getCellStatus() {
        return m_CellStatus;
    }

    @Override
    public String toString() {
        return "CellInfo{" +
                "Row=" + m_Row +
                ", Col=" + m_Col +
                ", CellStatus=" + m_CellStatus +
                '}';
    }

}
