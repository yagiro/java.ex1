package SupGame.Logic;

import java.util.Comparator;

/**
 * Created by Hagai on 10/08/2016.
 *
 */

public class StreakIdComparator implements Comparator<SupBoard.Streak> {

    @Override
    public int compare(SupBoard.Streak i_Streak1, SupBoard.Streak i_Streak2) {

        return i_Streak1.getId() - i_Streak2.getId();
    }


}
