package SupGame.Games.ConsoleSup;

import SupGame.Logic.CellInfo;
import SupGame.Logic.MoveInfo;
import SupGame.SupGame;

/**
 * Created by yakir on 8/1/16.
 *
 */
public class ConsoleSup extends SupGame {

    private ConsoleManager m_ConsoleManager;
    private MoveInfo m_MoveInfo;

    public ConsoleSup() {
        m_ConsoleManager = new ConsoleManager();
    }

    @Override
    public void Run() {
        init();
        run();
    }

    private void init() {
        m_IsActive = true;
    }

    private void run() {
        while (m_IsActive) {
            m_ConsoleManager.ShowMenu();
            handleMenuOption(m_ConsoleManager.GetMenuOptionFromUser());
        }
    }

    private void handleMenuOption(MenuOption i_Option) {
        switch (i_Option) {
            case ReadGameFile:
                readGameFile();
                break;
            case StartGame:
                startGame();
                break;
            case ShowBoard:
                showBoard();
                break;
            case MakeMove:
                makeMove();
                break;
            case ShowMovesHistory:
                showMovesHistory();
                break;
            case Undo:
                undoMove();
                break;
            case ShowStats:
                showGameStats();
                break;
            case Quit:
                quitGame();
                break;
        }
    }

    private void startGame() {
    }

    @Override
    protected void readGameFile() {
        super.readGameFile();
    }

    private void showBoard() {
        m_ConsoleManager.DrawBoard(m_Board);
    }

    private void makeMove() {
        m_MoveInfo = m_ConsoleManager.GetMoveFromPlayer();
        if (m_MoveInfo!=null){
            for(CellInfo cell : m_MoveInfo.getCellInfos()){
                System.out.println(cell);
            }
        }
        else{
            System.out.println("NO!");
        }
    }

    private void showMovesHistory() {

    }

    private void undoMove() {

    }

    private void showGameStats() {

    }

    private void quitGame() {
        System.out.println(System.getProperty("line.separator") + "Goodbye!");
        m_IsActive = false;
    }
}
