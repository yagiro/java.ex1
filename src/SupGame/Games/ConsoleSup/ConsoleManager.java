package SupGame.Games.ConsoleSup;

import SupGame.Logic.CellStatus;
import SupGame.Logic.MoveInfo;
import SupGame.Logic.SupBoard;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by yakir on 8/1/16.
 *
 */
public class ConsoleManager {

    private Scanner m_Scanner;
    private MenuOption m_OptionChosenByUser;


    public ConsoleManager() {
        m_Scanner = new Scanner(System.in);
    }

    public void ShowMenu() {
        System.out.print(System.getProperty("line.separator"));
        for(MenuOption option : MenuOption.values()) {
            System.out.println("[" + (option.ordinal() + 1) + "] " + option);
        }
        System.out.print(System.getProperty("line.separator"));
    }

    public MenuOption GetMenuOptionFromUser() {
        boolean inputIsValid = false;

        askUserForValidMenuOptionNumber();

        while (!inputIsValid) {
            inputIsValid = true;
            try {
                Integer userInput = Integer.parseInt(m_Scanner.nextLine()) - 1;
                m_OptionChosenByUser = MenuOption.values()[userInput]; // decrementing because enum starts at 0 and menu option at 1.
            }
            catch(Exception e) {
                ShowMenu();
                System.out.println("You entered an invalid input.");
                askUserForValidMenuOptionNumber();
                inputIsValid = false;
            }
        }

        return m_OptionChosenByUser;
    }

    private void askUserForValidMenuOptionNumber() {
        int lastMenuOptionIndex = MenuOption.values().length;
        System.out.print("Please pick a number in the range [ 1 - " + lastMenuOptionIndex + " ]: ");
    }

    public MenuOption getOptionChosenByUser() {
        return m_OptionChosenByUser;
    }

    public MoveInfo GetMoveFromPlayer() {
        String userInput;
        boolean isValidMoveString = false;
        MoveInfo moveInfo = null;

        System.out.print("Enter your move (SHOW HELP): ");

        while (!isValidMoveString) {
            isValidMoveString = true;
            userInput = m_Scanner.nextLine();
            moveInfo = MoveInfo.Parse(userInput);
            if(moveInfo == null) {
                isValidMoveString = false;
                System.out.print("Invalid move input." + System.getProperty("line.separator") +
                        "Please Re-enter your move: ");
            }
        }

        return moveInfo;
    }

    public void DrawBoard(SupBoard i_Board) {
        System.out.print(System.getProperty("line.separator")); // todo system.newline
        BoardDrawer.Draw(i_Board);
    }

    private static class BoardDrawer {

        private static int s_HeaderFrameLength;

        public static void Draw(SupBoard i_Board) {
            calcHeaderFrameLength(i_Board);
            BoardDrawer.DrawHeader(i_Board.getNumOfCols());
            BoardDrawer.DrawRows(i_Board);
            DrawColumnStreaks(i_Board.getColStreaks());
        }

        private static void calcHeaderFrameLength(SupBoard i_Board) {
            s_HeaderFrameLength = 1;
            int numOfCols = i_Board.getNumOfCols();

            for (int i = 1; i <= numOfCols ; ++i) {
                s_HeaderFrameLength += 4 ;
            }
        }

        public static void DrawHeader(int i_NumOfCols) {
            System.out.print("  |");
            for (int i = 1; i <= i_NumOfCols; ++i) {
                System.out.print(" " + i);
                if (i <= 9) {
                    System.out.print(" ");
                }
                System.out.print("|");
            }
            System.out.print(System.getProperty("line.separator") + "  ");

            DrawFrame();
        }

        public static void DrawFrame(){
            for (int i = 1; i <= s_HeaderFrameLength; ++i) {
                System.out.print("-");
            }
            System.out.print(System.getProperty("line.separator")); // todo: use System.Newline
        }


        public static void DrawRows(SupBoard i_Board) {

            SupBoard.Streak CurrentRowStreak;
            for (int i = 0; i < i_Board.getNumOfRows(); ++i) {
                CurrentRowStreak = i_Board.getRowStreaks().getStreaks().get(i);
                DrawRow(i + 1, i_Board.getCells()[i], CurrentRowStreak);
            }
            System.out.print("  ");
            DrawFrame();
        }

        private static void DrawRow(int i_RowNum, CellStatus[] i_Row, SupBoard.Streak m_RowStreak) {
            int colNum = 1;

            System.out.print(i_RowNum + " |");
            for (CellStatus cellStatus : i_Row) {
                System.out.print(cellStatus.getConsoleRepresentaion());
                System.out.print("|");
                colNum++;
            }

            // print current row's streak
            for(int block : m_RowStreak.getStreakLengths()){
                System.out.print(" " + block);
            }

            System.out.print(System.getProperty("line.separator")); // todo: System.Newline
        }

        private static void DrawColumnStreaks(SupBoard.Streaks i_ColumnStreaks){

            System.out.print("    ");
            for(int i = 0; i < i_ColumnStreaks.getMaxStreakLength(); i++){
                    for(SupBoard.Streak streak : i_ColumnStreaks.getStreaks()){
                        if(streak.getStreakLengths().length > i){
                            System.out.print(streak.getStreakLengths()[i] + "  ");
                            if(streak.getStreakLengths()[i] <= 9){
                                System.out.print(" ");  //this if is implemented to deal with double digit numbers in streaks
                            }
                        }
                        else{
                            System.out.print("    ");
                        }
                }
                System.out.print(System.getProperty("line.separator") + "    ");
            }
        }
    }
}
