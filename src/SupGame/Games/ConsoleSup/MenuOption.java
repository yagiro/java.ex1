package SupGame.Games.ConsoleSup;

/**
 * Created by yakir on 8/2/16.
 */
public enum MenuOption {
    ReadGameFile {
        @Override
        public String toString() {
            return "Read Game File";
        }
    }, StartGame {
        public String toString() {
            return "Start Game";
        }
    }, ShowBoard {
        public String toString() {
            return "Show SupBoard";
        }
    }, MakeMove {
        public String toString() {
            return "Make Move";
        }
    }, ShowMovesHistory {
        public String toString() {
            return "Show Moves History";
        }
    }, Undo {
        public String toString() {
            return "Undo";
        }
    }, ShowStats {
        public String toString() {
            return "Show Stats";
        }
    }, Quit {
        public String toString() {
            return "Quit";
        }
    };
}