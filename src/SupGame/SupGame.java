package SupGame;

import GameInfrastructure.Game;
import Generated.GameDescriptor;
import SupGame.Logic.Exceptions.InvalidGameDescriptorException;
import SupGame.Logic.SupBoard;
import SupGame.Logic.Validator;
import SupGame.Utils.XmlUtils;

import javax.xml.bind.JAXBException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yakir on 8/1/16.
 *
 */
public abstract class SupGame extends Game {
    protected Solution m_Solution;
    protected SupBoard m_Board;

    protected void readGameFile() {
        GameDescriptor gameDescriptor = null;

        try {
            gameDescriptor = XmlUtils.ParseGameDescriptorFromXml("src/resources/gd-example.xml");
            Validator.Validate(gameDescriptor);
        }
        catch(JAXBException e) {
            e.printStackTrace();
        }
        catch(InvalidGameDescriptorException e) {
            System.out.print(e.getMessage());
        }

        // At this stage we know gameDescriptor is valid (Logically and syntactly)
        populateMembers(gameDescriptor);
    }

    protected void populateMembers(GameDescriptor i_GameDescriptor) {
        m_Solution = new Solution(i_GameDescriptor.getBoard().getSolution());
        m_Board = new SupBoard(i_GameDescriptor.getBoard());
    }

    public class Solution {
        private ArrayList<SupGame.Solution.CellCoord> m_CellCoords;

        public Solution() {
            m_CellCoords = new ArrayList<>();
        }

        public Solution(Generated.Solution i_RawSolution) {
            this();

            List<Generated.Square> squares = i_RawSolution.getSquare();
            for (Generated.Square square : squares) {
                m_CellCoords.add(new CellCoord(square.getRow(), square.getColumn()));
            }
        }

        public ArrayList<SupGame.Solution.CellCoord> getCellCoords() {
            return m_CellCoords;
        }

        public class CellCoord {
            public BigInteger Row, Col;

            public CellCoord(BigInteger i_Row, BigInteger i_Col) {
                Row = i_Row;
                Col = i_Col;
            }
        }
    }
}
