package GameInfrastructure;

/**
 * Created by yakir on 8/1/16.
 */
public abstract class Game {
    protected boolean m_IsActive;
    protected InputManager m_InputManager;

    public abstract void Run();
}
