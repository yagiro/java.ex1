package GameInfrastructure;

/**
 * Created by yakir on 7/27/16.
 */
public class GameManager {
    private Game m_Game;

    public GameManager(Game i_Game) {
        m_Game = i_Game;
    }

    public void Run() {
        m_Game.Run();
    }
}
